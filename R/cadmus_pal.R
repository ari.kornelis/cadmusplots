#' Return function to interpolate a cadmus color palette
#'
#' Palettes include: cool1, cool2, cool3, cool4, cool5, cool6
#'
#'
#' @param palette Character name of palette in cadmus_palettes or custom color palette. e.g. palette = cadmus_cols("cool1", "cool2", etc.))
#' @param reverse Boolean indicating whether the palette should be reversed
#' @param ... Additional arguments to pass to colorRampPalette()
#' @export

cadmus_pal <- function(palette = "cool3", reverse = FALSE, ...) {
  cadmus_palettes <<- list(
    cool1 = cadmus_cols('Blue'),
    cool2 = cadmus_cols('Blue', 'Green'),
    cool3 = cadmus_cols('Blue', 'Green', 'Grey'),
    cool4 = cadmus_cols('Blue', 'Green', 'Cyan', 'Grey'),
    cool5 = cadmus_cols('Blue', 'Green', 'Cyan', 'Grey', 'Indigo'),
    cool6 = cadmus_cols('Blue', 'Green', 'Cyan', 'Grey', 'Indigo', 'Lavender'),

    # Old Palettes:
    `main`  = cadmus_cols("Blue", "Green", "Yellow", 'Grey', 'Cyan', 'Indigo'),
    `pal6`  = cadmus_cols("Blue", "Green", "Yellow", 'Grey', 'Cyan', 'Indigo'),
    `pal7`  = cadmus_cols("Blue", "Green", 'Grey', 'Cyan', 'Indigo', "Red", 'Rust'),
    `full`  = cadmus_cols("Blue", "Green", 'Red', "Yellow", 'Indigo', 'Rust',
                          'Cyan', 'Grey', 'Periwinkle', 'Brown', 'Magenta',
                          'Taupe', 'DarkGrey'),
    `cool`  = cadmus_cols("Blue", "Green"),
    `hot`   = cadmus_cols("Yellow", "Rust", "Magenta"),
    `mixed` = cadmus_cols("Blue", "Green", "Yellow", "Rust", "Red"),
    `grey`  = cadmus_cols("LightGrey", "DarkGrey")
  )

  if(any(palette %in% names(cadmus_palettes))) { pal <- cadmus_palettes[[palette]] }
  else { pal <- palette }

  if (reverse) pal <- rev(pal)

  colorRampPalette(pal, ...)
}
